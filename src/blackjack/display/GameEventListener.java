package blackjack.display;

import java.util.EventListener;

public interface GameEventListener extends EventListener {
    public void gameEvent(GameEvent event);
}
