package blackjack.display;

import javax.swing.*;
import java.awt.*;

public class Display {

    private JFrame frame;

//    private Canvas canvas;

    private final String title;
    private final int width;
    private final int height;

    public Display(String title, int width, int height) {
        this.title = title;
        this.width = width;
        this.height = height;
    }

    public Display(){
        this.title = "Blackjack";
        this.height = 1024;
        this.width = 768;
    }

    private void createDisplay(){
        frame = new JFrame(title);
        frame.setSize(width,height);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new BorderLayout());
    }


}
